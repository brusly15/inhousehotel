const roomList = {
    bachata1: ["B1-101", "B1-102", "B1-103", "B1-104", "B1-105", "B1-106", "B1-107", "B1-108", "B1-109", "B1-110", "B1-111", "B1-112", "B1-113", "B1-114", "B1-115", "B1-116", "B1-117", "B1-118", "B1-119", "B1-120", "B1-201", "B1-202", "B1-203", "B1-204", "B1-205", "B1-206", "B1-207", "B1-208", "B1-209", "B1-210", "B1-211", "B1-212", "B1-213", "B1-214", "B1-215", "B1-216", "B1-217", "B1-218", "B1-219", "B1-220", "B1-221", "B1-222", "B1-301", "B1-302", "B1-303", "B1-304", "B1-305", "B1-306", "B1-307", "B1-308", "B1-309", "B1-310", "B1-311", "B1-312", "B1-313", "B1-314", "B1-315", "B1-316", "B1-317", "B1-318", "B1-319", "B1-320", "B1-321", "B1-322"],
    bachata2: ["B2-101", "B2-102", "B2-103", "B2-104", "B2-105", "B2-106", "B2-107", "B2-108", "B2-109", "B2-110", "B2-111", "B2-112", "B2-113", "B2-114", "B2-115", "B2-116", "B2-117", "B2-118", "B2-119", "B2-120", "B2-201", "B2-202", "B2-203", "B2-204", "B2-205", "B2-206", "B2-207", "B2-208", "B2-209", "B2-210", "B2-211", "B2-212", "B2-213", "B2-214", "B2-215", "B2-216", "B2-217", "B2-218", "B2-219", "B2-220", "B2-221", "B2-222", "B2-301", "B2-302", "B2-303", "B2-304", "B2-305", "B2-306", "B2-307", "B2-308", "B2-309", "B2-310", "B2-311", "B2-312", "B2-313", "B2-314", "B2-315", "B2-316", "B2-317", "B2-318", "B2-319", "B2-320", "B2-321", "B2-322"],
    fantasia: ["F-101", "F-102", "F-103", "F-104", "F-105", "F-106", "F-107", "F-108", "F-109", "F-110", "F-111", "F-112", "F-113", "F-114", "F-115", "F-116", "F-117", "F-118", "F-119", "F-120", "F-201", "F-202", "F-203", "F-204", "F-205", "F-206", "F-207", "F-208", "F-209", "F-210", "F-211", "F-212", "F-213", "F-214", "F-215", "F-216", "F-217", "F-218", "F-219", "F-220", "F-221", "F-222", "F-301", "F-302", "F-303", "F-304", "F-305", "F-306", "F-307", "F-308", "F-309", "F-310", "F-311", "F-312", "F-313", "F-314", "F-315", "F-316", "F-317", "F-318", "F-319", "F-320", "F-321", "F-322"],
    salsa: ["S-101", "S-102", "S-103", "S-104", "S-105", "S-106", "S-107", "S-108", "S-109", "S-110", "S-111", "S-112", "S-113", "S-114", "S-115", "S-116", "S-117", "S-118", "S-119", "S-120", "S-201", "S-202", "S-203", "S-204", "S-205", "S-206", "S-207", "S-208", "S-209", "S-210", "S-211", "S-212", "S-213", "S-214", "S-215", "S-216", "S-217", "S-218", "S-219", "S-220", "S-221", "S-222", "S-301", "S-302", "S-303", "S-304", "S-305", "S-306", "S-307", "S-308", "S-309", "S-310", "S-311", "S-312", "S-313", "S-314", "S-315", "S-316", "S-317", "S-318", "S-319", "S-320", "S-321", "S-322"],
    merengue1: ["M1-101", "M1-102", "M1-103", "M1-104", "M1-105", "M1-106", "M1-107", "M1-108", "M1-109", "M1-110", "M1-111", "M1-112", "M1-113", "M1-114", "M1-115", "M1-116", "M1-117", "M1-118", "M1-119", "M1-120", "M1-201", "M1-202", "M1-203", "M1-204", "M1-205", "M1-206", "M1-207", "M1-208", "M1-209", "M1-210", "M1-211", "M1-212", "M1-213", "M1-214", "M1-215", "M1-216", "M1-217", "M1-218", "M1-219", "M1-220", "M1-221", "M1-222", "M1-301", "M1-302", "M1-303", "M1-304", "M1-305", "M1-306", "M1-307", "M1-308", "M1-309", "M1-310", "M1-311", "M1-312", "M1-313", "M1-314", "M1-315", "M1-316", "M1-317", "M1-318", "M1-319", "M1-320", "M1-321", "M1-322"],
    merengue2: ["M2-101", "M2-102", "M2-103", "M2-104", "M2-105", "M2-106", "M2-107", "M2-108", "M2-109", "M2-110", "M2-111", "M2-112", "M2-113", "M2-114", "M2-115", "M2-116", "M2-117", "M2-118", "M2-119", "M2-120", "M2-201", "M2-202", "M2-203", "M2-204", "M2-205", "M2-206", "M2-207", "M2-208", "M2-209", "M2-210", "M2-211", "M2-212", "M2-213", "M2-214", "M2-215", "M2-216", "M2-217", "M2-218", "M2-219", "M2-220", "M2-221", "M2-222", "M2-301", "M2-302", "M2-303", "M2-304", "M2-305", "M2-306", "M2-307", "M2-308", "M2-309", "M2-310", "M2-311", "M2-312", "M2-313", "M2-314", "M2-315", "M2-316", "M2-317", "M2-318", "M2-319", "M2-320", "M2-321", "M2-322"],
    palma4: ["P4-101", "P4-102", "P4-103", "P4-104", "P4-105", "P4-106", "P4-107", "P4-108", "P4-109", "P4-110", "P4-111", "P4-112", "P4-113", "P4-114", "P4-115", "P4-116", "P4-117", "P4-118", "P4-119", "P4-120", "P4-201", "P4-202", "P4-203", "P4-204", "P4-205", "P4-206", "P4-207", "P4-208", "P4-209", "P4-210", "P4-211", "P4-212", "P4-213", "P4-214", "P4-215", "P4-216", "P4-217", "P4-218", "P4-219", "P4-220", "P4-221", "P4-222", "P4-301", "P4-302", "P4-303", "P4-304", "P4-305", "P4-306", "P4-307", "P4-308", "P4-309", "P4-310", "P4-311", "P4-312", "P4-313", "P4-314", "P4-315", "P4-316", "P4-317", "P4-318", "P4-319", "P4-320", "P4-321", "P4-322"]
};


document.addEventListener('DOMContentLoaded', () => {
    const urlParams = new URLSearchParams(window.location.search);
    const pavilion = urlParams.get('pavilion');
    const room = urlParams.get('room');

    if (room) {
        const roomNumberElement = document.getElementById('roomNumber');
        const roomTitleElement = document.getElementById('roomTitle');
        if (roomNumberElement && roomTitleElement) {
            roomNumberElement.innerText = room;
            roomTitleElement.innerText = `Habitación ${room}`;
            loadPersonnelData(room);
            loadUnavailableStates(room);
        }
    }

    if (pavilion) {
        const pavilionTitleElement = document.getElementById('pavilionTitle');
        const pavilionHeaderElement = document.getElementById('pavilionHeader');
        const pavilionNameElement = document.getElementById('pavilionName');
        if (pavilionTitleElement && pavilionHeaderElement && pavilionNameElement) {
            pavilionTitleElement.innerText = `${pavilion.toUpperCase()} - Complejo`;
            pavilionHeaderElement.innerText = `${pavilion.toUpperCase()} - Lista de Habitaciones`;
            pavilionNameElement.innerText = pavilion.toUpperCase();
            loadRooms(pavilion);
            updateVacancies();
        }
    }

    const downloadExcelButton = document.getElementById('downloadExcelButton');
    if (downloadExcelButton) {
        downloadExcelButton.addEventListener('click', () => {
            downloadExcel(pavilion);
        });
    }

    const backButton = document.getElementById('backButton');
    if (backButton) {
        backButton.addEventListener('click', () => {
            if (pavilion) {
                window.location.href = `pabellon.html?pavilion=${pavilion}`;
            } else {
                window.location.href = 'index.html';
            }
        });
    }
});

function loadRooms(pavilion) {
    const roomsContainer = document.getElementById('roomsContainer');
    roomsContainer.innerHTML = '';

    roomList[pavilion].forEach(room => {
        const roomDiv = document.createElement('div');
        roomDiv.className = 'room';
        roomDiv.id = `room-${room}`;
        roomDiv.innerHTML = `
            <a href="habitacion.html?room=${room}&pavilion=${pavilion}">
                <div class="room-number">${room}</div>
            </a>
            <button class="room-block-button" onclick="toggleBlock('${room}')">Bloquear</button>
        `;
        roomsContainer.appendChild(roomDiv);

        updateRoomDisplay(room);
    });
}

function updateRoomDisplay(room) {
    const roomDiv = document.getElementById(`room-${room}`);
    if (!roomDiv) return;

    let dataStore = JSON.parse(localStorage.getItem('hotelDataStore')) || {};
    const personnelData = dataStore.personnel || [];
    const unavailableData = dataStore.unavailable || {};
    const blockedRooms = dataStore.blockedRooms || {};

    const occupants = personnelData.filter(person => person.room === room).length;
    const unavailableCount = unavailableData[room] ? unavailableData[room].length : 0;
    const totalCapacity = 6;
    const vacancies = totalCapacity - (occupants + unavailableCount);

    if (blockedRooms[room]) {
        roomDiv.classList.add('room-blocked');
        roomDiv.querySelector('.room-block-button').textContent = 'Desbloquear';
    } else if (unavailableCount + occupants >= totalCapacity) {
        roomDiv.className = 'room full';
    } else if (occupants === 0 && unavailableCount === 0) {
        roomDiv.className = 'room available';
    } else {
        roomDiv.className = 'room vacancy';
    }

    // Mostrar el botón "Bloquear" solo si hay 6 vacantes disponibles
    const blockButton = roomDiv.querySelector('.room-block-button');
    if (vacancies === totalCapacity) {
        if (!blockButton) {
            const button = document.createElement('button');
            button.className = 'room-block-button';
            button.textContent = 'Bloquear';
            button.onclick = () => toggleBlock(room);
            roomDiv.appendChild(button);
        }
    } else {
        if (blockButton) {
            blockButton.remove();
        }
    }
}


function toggleBlock(room) {
    const roomDiv = document.getElementById(`room-${room}`);
    const blockButton = roomDiv.querySelector('.room-block-button');
    let dataStore = JSON.parse(localStorage.getItem('hotelDataStore')) || {};

    if (!dataStore.blockedRooms) {
        dataStore.blockedRooms = {};
    }

    if (roomDiv.classList.contains('room-blocked')) {
        roomDiv.classList.remove('room-blocked');
        blockButton.textContent = 'Bloquear';
        delete dataStore.blockedRooms[room];
    } else {
        roomDiv.classList.add('room-blocked');
        blockButton.textContent = 'Desbloquear';
        dataStore.blockedRooms[room] = true;
    }

    localStorage.setItem('hotelDataStore', JSON.stringify(dataStore));
    updateVacancies();
}


function updateVacancies() {
    let dataStore = JSON.parse(localStorage.getItem('hotelDataStore')) || {};
    const personnelData = dataStore.personnel || [];
    const unavailableData = dataStore.unavailable || {};
    const blockedRooms = dataStore.blockedRooms || {};

    const pavilions = Object.keys(roomList);

    pavilions.forEach(pavilion => {
        const rooms = roomList[pavilion];
        const totalCapacity = rooms.length * 6;
        const occupied = personnelData.filter(person => rooms.includes(person.room)).length;
        const unavailable = rooms.reduce((count, room) => count + (unavailableData[room] ? unavailableData[room].length : 0), 0);
        const blocked = Object.keys(blockedRooms).filter(room => rooms.includes(room)).length * 6;
        const available = totalCapacity - (occupied + unavailable + blocked);

        const vacancyElement = document.getElementById(`${pavilion}-vacancies`);
        if (vacancyElement) {
            vacancyElement.innerText = available;
        }
    });
}

function downloadExcel(pavilion) {
    const dataStore = JSON.parse(localStorage.getItem('hotelDataStore')) || {};
    const personnelData = dataStore.personnel || [];
    const observationsData = dataStore.observations || {};

    const data = [];
    const rooms = roomList[pavilion];
    if (!rooms) {
        console.error(`El pabellón ${pavilion} no existe en roomList.`);
        return;
    }

    rooms.forEach(room => {
        const roomPersonnel = personnelData.filter(person => person.room === room);

        if (roomPersonnel.length > 0) {
            roomPersonnel.forEach(person => {
                const observations = observationsData[room] ? observationsData[room][person.index] || [] : [];

                data.push({
                    Habitación: room,
                    Hotel: person.hotel || '',
                    Código: person.code || '',
                    Nombre: person.firstName || '',
                    Apellido: person.lastName || '',
                    Departamento: person.department || '',
                    Posición: person.position || '',
                    'Fecha de ingreso': person.date || '',
                    Almohadas: person.almohadaQty || 0,
                    Sábanas: person.sabanasQty || 0,
                    Observaciones: observations.length > 0 ? observations.map(obs => `${obs.timestamp}: ${obs.text}`).join('; ') : 'VACANTE'
                });
            });
        } else {
            data.push({
                Habitación: room,
                Hotel: '',
                Código: '',
                Nombre: '',
                Apellido: '',
                Departamento: '',
                Posición: '',
                'Fecha de ingreso': '',
                Almohadas: 0,
                Sábanas: 0,
                Observaciones: 'VACANTE'
            });
        }
    });

    const worksheet = XLSX.utils.json_to_sheet(data);
    const workbook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workbook, worksheet, 'Datos del Pabellón');

    XLSX.writeFile(workbook, `${pavilion}_datos.xlsx`);
}


function loadExcelData(event) {
    const file = event.target.files[0];
    const reader = new FileReader();

    reader.onload = (e) => {
        const data = new Uint8Array(e.target.result);
        const workbook = XLSX.read(data, { type: 'array' });

        const sheetName = workbook.SheetNames[0];
        const worksheet = workbook.Sheets[sheetName];
        const json = XLSX.utils.sheet_to_json(worksheet);

        let dataStore = JSON.parse(localStorage.getItem('hotelDataStore')) || {};
        if (!dataStore.personnel) {
            dataStore.personnel = [];
        }

        json.forEach(row => {
            if (row.Hotel !== 'VACANTE') {
                const person = {
                    room: row.Habitación,
                    hotel: row.Hotel,
                    code: row.Código,
                    firstName: row.Nombre,
                    lastName: row.Apellido,
                    department: row.Departamento,
                    position: row.Posición,
                    date: row['Fecha de ingreso'],
                    almohadaQty: row.Almohadas,
                    sabanasQty: row.Sábanas,
                    index: 0 // Actualiza esto según sea necesario para manejar múltiples ocupantes
                };
                dataStore.personnel.push(person);
            }
        });

        localStorage.setItem('hotelDataStore', JSON.stringify(dataStore));
        location.reload();
    };

    reader.readAsArrayBuffer(file);
}



function loadPersonnelData(room) {
    const personnelContainer = document.getElementById('personnelContainer');
    if (!personnelContainer) return;
    personnelContainer.innerHTML = '';

    const dataStore = JSON.parse(localStorage.getItem('hotelDataStore')) || {};
    const personnelData = dataStore.personnel || [];
    const observationsData = dataStore.observations || {};
    const roomPersonnel = personnelData.filter(person => person.room === room);

    for (let i = 0; i < 6; i++) {
        const person = roomPersonnel.find(p => p.index === i);
        const personCard = document.createElement('div');
        personCard.className = 'personnel-card';

        if (person) {
            personCard.innerHTML = `
                <div class="personnel-info">
                    <p><strong>Hotel:</strong> ${person.hotel}</p>
                    <p><strong>Código:</strong> ${person.code}</p>
                    <p><strong>Nombre:</strong> ${person.firstName}</p>
                    <p><strong>Apellido:</strong> ${person.lastName}</p>
                    <p><strong>Departamento:</strong> ${person.department}</p>
                    <p><strong>Posición:</strong> ${person.position}</p>
                    <p><strong>Fecha:</strong> ${person.date}</p>
                </div>
                <div class="personnel-options">
                    <button onclick="deletePersonnel('${person.code}', ${i})">Eliminar</button>
                    <button onclick="openObservationModal(${i}, '${person.code}', '${person.hotel}', '${person.firstName}', '${person.lastName}', '${person.department}', '${person.position}', '${person.date}')">Observación</button>
                    <button onclick="openUtensilsModal('${person.code}', ${i})">Utensilios</button>
                </div>
            `;

            // Aplicar la clase red-border si hay observaciones
            if (observationsData[room] && observationsData[room][i] && observationsData[room][i].length > 0) {
                personCard.classList.add('red-border');
            }
        } else {
            personCard.classList.add('vacante');
            personCard.innerHTML = `
                <p class="empty">Vacante</p>
                <div class="personnel-options">
                    <button onclick="markUnavailable(this)">No Disponible</button>
                </div>
            `;
        }

        personnelContainer.appendChild(personCard);
    }

    const assignButton = document.getElementById('assignButton');
    if (assignButton) {
        assignButton.style.display = roomPersonnel.length < 6 ? 'block' : 'none';
    }
}

function openUtensilsModal(code, index) {
    const room = document.getElementById('roomNumber').innerText;
    let dataStore = JSON.parse(localStorage.getItem('hotelDataStore')) || {};
    const person = dataStore.personnel.find(p => p.room === room && p.code === code && p.index == index);

    if (person) {
        document.getElementById('almohadaQty').value = person.almohadaQty || 0;
        document.getElementById('sabanasQty').value = person.sabanasQty || 0;
    } else {
        document.getElementById('almohadaQty').value = 0;
        document.getElementById('sabanasQty').value = 0;
    }

    document.getElementById('utensilsModal').dataset.personCode = code;
    document.getElementById('utensilsModal').dataset.personIndex = index;
    document.getElementById('utensilsModal').style.display = 'block';
}


function closeUtensilsModal() {
    document.getElementById('utensilsModal').style.display = 'none';
}

function search(event) {
    event.preventDefault();
    const query = document.getElementById('searchQuery').value.trim().toLowerCase();

    const dataStore = JSON.parse(localStorage.getItem('hotelDataStore')) || {};
    const allPersonnelData = dataStore.personnel || [];

    const results = allPersonnelData.filter(person =>
        (person.code && person.code.toLowerCase().includes(query)) ||
        (person.room && person.room.toLowerCase().includes(query))
    );

    if (results.length > 0) {
        const result = results[0];
        const room = result.room.toLowerCase();
        let pavilion;

        if (room.includes('b1')) {
            pavilion = 'bachata1';
        } else if (room.includes('b2')) {
            pavilion = 'bachata2';
        } else if (room.includes('f')) {
            pavilion = 'fantasia';
        } else if (room.includes('s')) {
            pavilion = 'salsa';
        } else if (room.includes('m1')) {
            pavilion = 'merengue1';
        } else if (room.includes('m2')) {
            pavilion = 'merengue2';
        } else {
            pavilion = 'palma4';
        }

        window.location.href = `habitacion.html?room=${result.room}&pavilion=${pavilion}`;
    } else {
        alert('No se encontraron resultados.');
    }
}

function showDeletedRecordsModal() {
    let dataStore = JSON.parse(localStorage.getItem('hotelDataStore')) || {};
    const deletedRecords = dataStore.deletedRecords || [];
    const deletedRecordsList = document.getElementById('deletedRecordsList');
    deletedRecordsList.innerHTML = '';

    if (deletedRecords.length === 0) {
        deletedRecordsList.innerHTML = '<tr><td colspan="8">No hay registros de entregas.</td></tr>';
    } else {
        deletedRecords.forEach(record => {
            const listItem = document.createElement('tr');
            listItem.innerHTML = `
                <td>${record.deletedAt}</td>
                <td>${record.hotel}</td>
                <td>${record.code}</td>
                <td>${record.firstName} ${record.lastName}</td>
                <td>${record.department}</td>
                <td>${record.position}</td>
                <td>${record.date}</td>
                <td>${record.room}</td>
            `;
            deletedRecordsList.appendChild(listItem);
        });
    }

    document.getElementById('deletedRecordsModal').style.display = 'block';
}

function closeDeletedRecordsModal() {
    document.getElementById('deletedRecordsModal').style.display = 'none';
}

function clearDeletedRecords() {
    if (confirm("¿Estás seguro de que deseas borrar todos los registros de entregas?")) {
        let dataStore = JSON.parse(localStorage.getItem('hotelDataStore')) || {};
        dataStore.deletedRecords = [];
        localStorage.setItem('hotelDataStore', JSON.stringify(dataStore));
        showDeletedRecordsModal();
    }
}

function openPersonnelModal() {
    document.getElementById('personnelModal').style.display = 'block';
}

function closePersonnelModal() {
    document.getElementById('personnelModal').style.display = 'none';
}

function openObservationModal(index, code, hotel, firstName, lastName, department, position, date) {
    const room = document.getElementById('roomNumber').innerText;
    let dataStore = JSON.parse(localStorage.getItem('hotelDataStore')) || {};
    const observationsData = dataStore.observations || {};

    const observations = observationsData[room] ? observationsData[room][index] || [] : [];

    document.getElementById('observationIndex').value = index;
    document.getElementById('observationText').value = '';
    document.getElementById('observationModal').style.display = 'block';

    const personInfo = document.getElementById('personInfo');
    personInfo.innerHTML = `
        <p><strong>Hotel:</strong> ${hotel}</p>
        <p><strong>Código:</strong> ${code}</p>
        <p><strong>Nombre:</strong> ${firstName} ${lastName}</p>
        <p><strong>Departamento:</strong> ${department}</p>
        <p><strong>Posición:</strong> ${position}</p>
        <p><strong>Fecha:</strong> ${date}</p>
    `;

    // Cargar observaciones previas
    const observationsList = document.getElementById('observationsList');
    observationsList.innerHTML = '';
    observations.forEach(observation => {
        const li = document.createElement('li');
        li.innerHTML = `<strong>Observación:</strong> ${observation.text} <br> <strong>Fecha y Hora:</strong> ${observation.timestamp}`;
        observationsList.prepend(li); // Insertar al principio para mantener el orden de más nuevo a más viejo
    });
}


function closeObservationModal() {
    const observationModal = document.getElementById('observationModal');
    if (observationModal) {
        observationModal.style.display = 'none';
    }
}


function saveObservation(event) {
    event.preventDefault(); // Prevenir el comportamiento por defecto del formulario

    const room = document.getElementById('roomNumber').innerText;
    const observationText = document.getElementById('observationText').value;
    const index = document.getElementById('observationIndex').value;
    const timestamp = new Date().toLocaleString();

    let dataStore = JSON.parse(localStorage.getItem('hotelDataStore')) || {};
    if (!dataStore.observations) {
        dataStore.observations = {};
    }

    if (!dataStore.observations[room]) {
        dataStore.observations[room] = {};
    }

    if (!dataStore.observations[room][index]) {
        dataStore.observations[room][index] = [];
    }

    dataStore.observations[room][index].unshift({ text: observationText, timestamp: timestamp }); // Agregar al principio de la lista

    localStorage.setItem('hotelDataStore', JSON.stringify(dataStore));

    // Agregar la clase red-border a la casilla correspondiente
    const personnelCard = document.querySelector(`#personnelContainer .personnel-card:nth-child(${index + 1})`);
    if (personnelCard) {
        personnelCard.classList.add('red-border');
    }

    closeObservationModal();
    location.reload();
}



function loadObservationList() {
    const room = document.getElementById('roomNumber').innerText;
    const index = document.getElementById('observationIndex').value;
    const observationsList = document.getElementById('observationsList');
    observationsList.innerHTML = '';
    const dataStore = JSON.parse(localStorage.getItem('hotelDataStore')) || {};
    const observations = dataStore.observations || {};
    if (observations[room] && observations[index]) {
        observations[room][index].forEach((obs, i) => {
            const listItem = document.createElement('li');
            listItem.innerHTML = `<strong>${i + 1}.</strong> ${obs.text} <em>(Agregado el ${obs.date})</em>`;
            observationsList.appendChild(listItem);
        });
    }
}

function assignPersonnel(event) {
    event.preventDefault();

    const code = document.getElementById('code').value.trim();
    const hotel = document.getElementById('hotel').value.trim();
    const firstName = document.getElementById('firstName').value.trim();
    const lastName = document.getElementById('lastName').value.trim();
    const department = document.getElementById('department').value.trim();
    const position = document.getElementById('position').value.trim();
    const date = document.getElementById('date').value.trim();

    if (!code || !hotel || !firstName || !lastName || !department || !position || !date) {
        alert("Por favor, complete todos los campos.");
        return;
    }

    const urlParams = new URLSearchParams(window.location.search);
    const room = urlParams.get('room');

    const dataStore = JSON.parse(localStorage.getItem('hotelDataStore')) || {};
    const personnelData = dataStore.personnel || [];

    const personnelContainer = document.getElementById('personnelContainer');
    const vacancyIndex = Array.from(personnelContainer.children).findIndex(card => card.classList.contains('vacante') && !card.classList.contains('no-disponible'));

    if (vacancyIndex !== -1) {
        personnelData.push({ code, hotel, firstName, lastName, department, position, room, date, index: vacancyIndex });
        dataStore.personnel = personnelData;
        localStorage.setItem('hotelDataStore', JSON.stringify(dataStore));
        loadPersonnelData(room);
        closePersonnelModal();
    } else {
        alert("No hay casillas vacantes disponibles.");
    }
}

function deletePersonnel(code, index) {
    const room = document.getElementById('roomNumber').innerText;

    let dataStore = JSON.parse(localStorage.getItem('hotelDataStore')) || {};
    let personnelData = dataStore.personnel || [];

    const personToDelete = personnelData.find(person => person.code === code && person.index === index);
    personnelData = personnelData.filter(person => !(person.code === code && person.index === index));
    dataStore.personnel = personnelData;

    if (!dataStore.deletedRecords) {
        dataStore.deletedRecords = [];
    }

    if (personToDelete) {
        dataStore.deletedRecords.push({
            deletedAt: new Date().toLocaleString(),
            ...personToDelete
        });
    }

    localStorage.setItem('hotelDataStore', JSON.stringify(dataStore));

    let observations = dataStore.observations || {};
    if (observations[room] && observations[room][index]) {
        delete observations[room][index];
        dataStore.observations = observations;
        localStorage.setItem('hotelDataStore', JSON.stringify(dataStore));
    }

    loadPersonnelData(room);
}
function markUnavailable(button) {
    const personCard = button.closest('.personnel-card');
    const index = Array.from(personCard.parentNode.children).indexOf(personCard);
    personCard.classList.add('no-disponible');
    personCard.querySelector('.empty').innerText = "No Disponible";
    button.outerHTML = '<button onclick="markAvailable(this)">Disponible</button>';
    saveUnavailableState(index);
}

function markAvailable(button) {
    const personCard = button.closest('.personnel-card');
    const index = Array.from(personCard.parentNode.children).indexOf(personCard);
    personCard.classList.remove('no-disponible');
    personCard.querySelector('.empty').innerText = "Vacante";
    button.outerHTML = '<button onclick="markUnavailable(this)">No Disponible</button>';
    removeUnavailableState(index);
}

function saveUnavailableState(index) {
    const room = document.getElementById('roomNumber').innerText;
    let dataStore = JSON.parse(localStorage.getItem('hotelDataStore')) || {};
    if (!dataStore.unavailable) {
        dataStore.unavailable = {};
    }
    if (!dataStore.unavailable[room]) {
        dataStore.unavailable[room] = [];
    }
    if (!dataStore.unavailable[room].includes(index)) {
        dataStore.unavailable[room].push(index);
    }
    localStorage.setItem('hotelDataStore', JSON.stringify(dataStore));
}

function removeUnavailableState(index) {
    const room = document.getElementById('roomNumber').innerText;
    let dataStore = JSON.parse(localStorage.getItem('hotelDataStore')) || {};
    if (dataStore.unavailable && dataStore.unavailable[room]) {
        dataStore.unavailable[room] = dataStore.unavailable[room].filter(i => i !== index);
        localStorage.setItem('hotelDataStore', JSON.stringify(dataStore));
    }
}

function loadUnavailableStates(room) {
    const personnelContainer = document.getElementById('personnelContainer');
    let dataStore = JSON.parse(localStorage.getItem('hotelDataStore')) || {};
    const unavailableStates = dataStore.unavailable || {};
    if (unavailableStates[room]) {
        unavailableStates[room].forEach(index => {
            const card = personnelContainer.children[index];
            if (card && card.classList.contains('vacante')) {
                card.classList.add('no-disponible');
                card.querySelector('.empty').innerText = "No Disponible";
                const button = card.querySelector('.personnel-options button');
                if (button) {
                    button.outerHTML = '<button onclick="markAvailable(this)">Disponible</button>';
                }
            }
        });
    }
}

function checkAvailability() {
    const personnelContainer = document.getElementById('personnelContainer');
    const availableCards = personnelContainer.querySelectorAll('.vacante:not(.no-disponible)');
    if (availableCards.length > 0) {
        openPersonnelModal();
    } else {
        alert("NO HAY DISPONIBILIDAD");
    }
}

function updateVacancies() {
    let dataStore = JSON.parse(localStorage.getItem('hotelDataStore')) || {};
    const personnelData = dataStore.personnel || [];
    const unavailableData = dataStore.unavailable || {};
    const blockedRooms = dataStore.blockedRooms || {};

    const pavilions = Object.keys(roomList);

    pavilions.forEach(pavilion => {
        const rooms = roomList[pavilion];
        const totalCapacity = rooms.length * 6; 
        const occupied = personnelData.filter(person => rooms.includes(person.room)).length;
        const unavailable = rooms.reduce((count, room) => count + (unavailableData[room] ? unavailableData[room].length : 0), 0);
        const blocked = Object.keys(blockedRooms).filter(room => rooms.includes(room)).length * 6; 
        const available = totalCapacity - (occupied + unavailable + blocked);

        const vacancyElement = document.getElementById(`${pavilion}-vacancies`);
        if (vacancyElement) {
            vacancyElement.innerText = available;
        }
    });
}


function generatePavilionStatusChart(type, data) {
    const ctx = document.getElementById('statusChart').getContext('2d');
    if (window.pavilionStatusChart) {
        window.pavilionStatusChart.destroy();
    }
    window.pavilionStatusChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: Object.keys(data),
            datasets: [{
                label: `Ocupación por ${type === 'hotel' ? 'hotel' : 'posición'}`,
                data: Object.values(data),
                backgroundColor: 'rgba(54, 162, 235, 0.2)',
                borderColor: 'rgba(54, 162, 235, 1)',
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
}

function showPavilionStatus(pavilion) {
    const dataStore = JSON.parse(localStorage.getItem('hotelDataStore')) || {};
    const personnelData = dataStore.personnel || [];
    const rooms = roomList[pavilion] || [];

    const statusByHotel = {};
    const statusByPosition = {};

    rooms.forEach(room => {
        const roomPersonnel = personnelData.filter(person => person.room === room);

        roomPersonnel.forEach(person => {
            if (person.hotel && person.hotel !== "undefined") {
                if (!statusByHotel[person.hotel]) {
                    statusByHotel[person.hotel] = 0;
                }
                statusByHotel[person.hotel]++;
            }

            if (person.position && person.position !== "undefined") {
                if (!statusByPosition[person.position]) {
                    statusByPosition[person.position] = 0;
                }
                statusByPosition[person.position]++;
            }
        });
    });

    const pavilionStatusModal = document.getElementById('pavilionStatusModal');
    const statusChart = document.getElementById('statusChart');
    if (pavilionStatusModal && statusChart) {
        pavilionStatusModal.style.display = 'block';
        const statusCategory = document.getElementById('statusCategory');
        if (statusCategory) {
            statusCategory.value = 'hotel';
        }
        generatePavilionStatusChart('hotel', statusByHotel);
    } else {
        console.error('El modal de estado del pabellón o el gráfico no están presentes en el DOM.');
    }
}


function closePavilionStatusModal() {
    const pavilionStatusModal = document.getElementById('pavilionStatusModal');
    if (pavilionStatusModal) {
        pavilionStatusModal.style.display = 'none';
        location.reload();
    }
}



function updateChart(type) {
    const dataStore = JSON.parse(localStorage.getItem('hotelDataStore')) || {};
    const personnelData = dataStore.personnel || [];

    const statusByHotel = {};
    const statusByPosition = {};

    personnelData.forEach(person => {
        if (type === 'hotel' && person.hotel && person.hotel !== "undefined") {
            if (!statusByHotel[person.hotel]) {
                statusByHotel[person.hotel] = 0;
            }
            statusByHotel[person.hotel]++;
        } else if (type === 'position' && person.position && person.position !== "undefined") {
            if (!statusByPosition[person.position]) {
                statusByPosition[person.position] = 0;
            }
            statusByPosition[person.position]++;
        }
    });

    const data = type === 'hotel' ? statusByHotel : statusByPosition;
    generatePavilionStatusChart(type, data);
}


function openUtensilsModal(code, index) {
    const room = document.getElementById('roomNumber').innerText;
    let dataStore = JSON.parse(localStorage.getItem('hotelDataStore')) || {};
    const person = dataStore.personnel.find(p => p.room === room && p.code === code && p.index == index);

    if (person) {
        document.getElementById('almohadaQty').value = person.almohadaQty || 0;
        document.getElementById('sabanasQty').value = person.sabanasQty || 0;
    } else {
        document.getElementById('almohadaQty').value = 0;
        document.getElementById('sabanasQty').value = 0;
    }

    const utensilsModal = document.getElementById('utensilsModal');
    if (utensilsModal) {
        utensilsModal.dataset.personCode = code;
        utensilsModal.dataset.personIndex = index;
        utensilsModal.style.display = 'block';
    }
}


function closeUtensilsModal() {
    const utensilsModal = document.getElementById('utensilsModal');
    if (utensilsModal) {
        utensilsModal.style.display = 'none';
    }
}


function saveUtensils() {
    const room = document.getElementById('roomNumber').innerText;
    const almohadaQty = parseInt(document.getElementById('almohadaQty').value, 10);
    const sabanasQty = parseInt(document.getElementById('sabanasQty').value, 10);

    const personCode = document.getElementById('utensilsModal').dataset.personCode;
    const personIndex = document.getElementById('utensilsModal').dataset.personIndex;

    let dataStore = JSON.parse(localStorage.getItem('hotelDataStore')) || {};
    if (!dataStore.personnel) {
        dataStore.personnel = [];
    }

    let person = dataStore.personnel.find(p => p.room === room && p.code === personCode && p.index == personIndex);
    if (person) {
        person.almohadaQty = almohadaQty;
        person.sabanasQty = sabanasQty;
    } else {
        person = { room, code: personCode, index: personIndex, almohadaQty, sabanasQty };
        dataStore.personnel.push(person);
    }

    localStorage.setItem('hotelDataStore', JSON.stringify(dataStore));
    closeUtensilsModal();
}


function loadUtensilsData() {
    const room = document.getElementById('roomNumber').innerText;
    let dataStore = JSON.parse(localStorage.getItem('hotelDataStore')) || {};
    const utensilsData = dataStore.utensils && dataStore.utensils[room] ? dataStore.utensils[room] : { almohadaQty: 0, sabanasQty: 0 };

    document.getElementById('almohadaQty').value = utensilsData.almohadaQty;
    document.getElementById('sabanasQty').value = utensilsData.sabanasQty;
}
